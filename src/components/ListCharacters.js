import React from 'react'
import { useState, useEffect } from 'react';
import axios from 'axios';

const ListCharacters = () => {
    const [characters, setCharacters] = useState([]);

  useEffect(() => {
    axios.get('https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=deaa6f0e20bb3598695c79544c27c741&hash=e56fcd8edde27e2995d3f773311375a4').then(res=>{
        setCharacters(res.data.data.results)
    }).catch(error=>console.log(error))
  },[])
console.log(characters)
  return (
    <div className="App container">
      <h1 className="container" style={{textAlign: "center", marginBottom: "15px", marginTop: "15px"}}>List of Marvel Characters</h1>
        <table className="table table-dark table-hover">
            <thead>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Foto</th>
                    <th scope="col">Nombre</th>
                </tr>
            </thead>
            
            { characters.map(characters=>(
            <tbody key={characters.id}>
                <tr>
                        <th scope="row">{characters.id}</th>
                        <td><img style={{width: "10rem", height: "10rem"}} src={`${characters.thumbnail.path}.${characters.thumbnail.extension}`} className="card-img-top"/></td>
                        <td>{characters.name}</td>
                </tr>
            </tbody>
            ))
            }
        </table>
    </div>


        
  );
}

export default ListCharacters 