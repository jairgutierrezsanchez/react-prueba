import React from 'react'
import { useState, useEffect } from 'react';
import axios from 'axios';

const CardsCharacters = () => {
    const [characters, setCharacters] = useState([]);

  useEffect(() => {
    axios.get('https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=deaa6f0e20bb3598695c79544c27c741&hash=e56fcd8edde27e2995d3f773311375a4').then(res=>{
        setCharacters(res.data.data.results)
    }).catch(error=>console.log(error))
  },[])
console.log(characters)
  return (
    <div className="App container">
      <h1 className="container" style={{textAlign: "center", marginBottom: "15px", marginTop: "15px"}}>Cards of Marvel characters</h1>

<div className="row row-cols-1 row-cols-md-3 g-4 mb-5">
        { characters.map(characters=>(
            <div className="col" key={characters.id}>
                <div className="card" style={{width: "18rem", height: "28rem"}}>
                    <img src={`${characters.thumbnail.path}.${characters.thumbnail.extension}`} className="card-img-top"/>
                    <div className="card-body">
                        <p className="card-text">{characters.name}</p>
                    </div>
                </div>
            </div>
        ))
        
        }
    </div>

    </div>
  );
}

export default CardsCharacters 