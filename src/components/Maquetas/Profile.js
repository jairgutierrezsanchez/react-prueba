import React from 'react'

import { Outlet, Link } from 'react-router-dom'

import perfil from '../../assets/img/profile.png'
import logo from '../../assets/img/logo.png'

const Profile = () => {

    return (
      <div className="App profile">
          <div className="profile-profile_container">
            <section className="profile-profile_container-profile_sectionIzq">
                <div className="profile-profile_container-profile_sectionIzq-profile_logo">
                    <img className="profile-profile_container-profile_sectionIzq-profile_logo-profile_fotoLogo" src={logo} />
                </div>
                <div className="profile-profile_container-profile_sectionIzq-profile_title"><h3>¡Ingresa a tu cuenta!</h3></div>
                <div className="profile-profile_container-profile_sectionIzq-profile_social mb-3">
                    <div className="profile-profile_container-profile_sectionIzq-profile_social-profileGoogle">
                        <button type="submit" className="btn profile-profile_container-profile_sectionIzq-profile_social-profileGoogle-profile_buttons">
                            <section className="profile-profile_container-profile_sectionIzq-profile_social-profileGoogle-profile_buttons-profile_letraG">G</section> 
                            <section className="profile-profile_container-profile_sectionIzq-profile_social-profileGoogle-profile_buttons-profile_letra">Iniciar con Google</section>
                        </button>
                    </div>
                    <div className="profile-profile_container-profile_sectionIzq-profile_social-profileFacebook">
                        <button type="submit" className="btn profile-profile_container-profile_sectionIzq-profile_social-profileFacebook-profile_buttons">
                            <div className="profile-profile_container-profile_sectionIzq-profile_social-profileFacebook-profile_buttons-profile_letraF">F</div>
                            <div className="profile-profile_container-profile_sectionIzq-profile_social-profileFacebook-profile_buttons-profile_letra">Iniciar con Facebook</div>
                        </button>
                    </div>
                </div>
                <div>
                    <form className="profile-profile_container-profile_sectionIzq-profile_form">
                        <div className="mb-3">
                            <label for="exampleInputEmail1" className="form-label">Correo electrónico</label>
                            <input type="email" className="form-control profile-profile_container-profile_sectionIzq-profile_form-profile_input" 
                                    id="exampleInputEmail1" aria-describedby="emailHelp"/>
                        </div>
                        <div className="mb-3">
                            <label for="exampleInputPassword1" className="form-label">Contraseña</label>
                            <input type="password" className="form-control profile-profile_container-profile_sectionIzq-profile_form-profile_input" 
                                    id="exampleInputPassword1"/>
                        </div>
                        <div className="profile-profile_container-profile_sectionIzq-profile_form-profile_buttons">
                            <button type="submit" className="btn profile-profile_container-profile_sectionIzq-profile_form-profile_buttons-profile_login">Iniciar sesión</button>
                            <button type="submit" className="btn profile-profile_container-profile_sectionIzq-profile_form-profile_buttons-profile_create">Crear usuario</button>
                        </div>
                    </form>
                </div>
                <div className="profile-profile_container-profile_sectionIzq-profile_password">No recuerdas tu contraseña? 
                    <Link className="profile-profile_container-profile_sectionIzq-profile_password-profile_link"  
                            to="/./Profile"> Olvide contraseña</Link></div>
            </section>
            <section className="profile-profile_container-profile_sectionDer">
                <div className="profile-profile_container-profile_sectionDer-profile_img">
                    <img className="profile-profile_container-profile_sectionDer-profile_img-profile_foto" src={perfil} />
                </div>
            </section>
          </div>
      </div>
    ); 
  }
  
  export default Profile 


