import { CircularProgress } from "@mui/material";

export function CardsCarousel(){
    return (
        <div className="dashboard">
            <h4 className="dashboard-dash_title">CURSOS DE DIANNE AMTER</h4>
            <div className="dashboard-dash_cardsCursos">
                <div className="dashboard-dash_cardsCursos-cursos">
                    <section className="dashboard-dash_cardsCursos-cursos-arriba">
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-barra">
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-izq">
                                <CircularProgress variant="determinate" value={80} />
                            </section>
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-der">
                                <div>
                                    Curso...
                                </div>
                                <div>
                                    80%
                                </div>
                            </section>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-icon">
                            <i class="fas fa-heart"></i>
                        </div>
                    </section>
                    <section className="dashboard-dash_cardsCursos-cursos-abajo">
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            12
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backUno">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            2
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backDos">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            3
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backTres">
                                <i class="fas fa-gem"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            15
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backCuatro">
                                <i class="fas fa-fire"></i>
                            </div>
                        </div>
                    </section>
                </div>
                <div className="dashboard-dash_cardsCursos-cursos">
                    <section className="dashboard-dash_cardsCursos-cursos-arriba">
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-barra">
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-izq">
                                <CircularProgress variant="determinate" value={80} />
                            </section>
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-der">
                                <div>
                                    Curso...
                                </div>
                                <div>
                                    70%
                                </div>
                            </section>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-icon">
                            <i class="fas fa-lungs"></i>
                        </div>
                    </section>
                    <section className="dashboard-dash_cardsCursos-cursos-abajo">
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            12
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backUno">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            2
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backDos">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            3
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backTres">
                                <i class="fas fa-gem"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            15
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backCuatro">
                                <i class="fas fa-fire"></i>
                            </div>
                        </div>
                    </section>
                </div>
                <div className="dashboard-dash_cardsCursos-cursos">
                    <section className="dashboard-dash_cardsCursos-cursos-arriba">
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-barra">
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-izq">
                                <CircularProgress variant="determinate" value={80} />
                            </section>
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-der">
                                <div>
                                    Curso...
                                </div>
                                <div>
                                    85%
                                </div>
                            </section>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-icon">
                            <i class="fas fa-heart"></i>
                        </div>
                    </section>
                    <section className="dashboard-dash_cardsCursos-cursos-abajo">
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            12
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backUno">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            2
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backDos">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            3
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backTres">
                                <i class="fas fa-gem"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            15
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backCuatro">
                                <i class="fas fa-fire"></i>
                            </div>
                        </div>
                    </section>
                </div>
                <div className="dashboard-dash_cardsCursos-cursos">
                    <section className="dashboard-dash_cardsCursos-cursos-arriba">
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-barra">
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-izq">
                                <CircularProgress variant="determinate" value={80} />
                            </section>
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-der">
                                <div>
                                    Curso...
                                </div>
                                <div>
                                    90%
                                </div>
                            </section>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-icon">
                            <i class="fas fa-stomach"></i>
                        </div>
                    </section>
                    <section className="dashboard-dash_cardsCursos-cursos-abajo">
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            12
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backUno">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            2
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backDos">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            3
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backTres">
                                <i class="fas fa-gem"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            15
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backCuatro">
                                <i class="fas fa-fire"></i>
                            </div>
                        </div>
                    </section>
                </div>
                <div className="dashboard-dash_cardsCursos-cursos">
                    <section className="dashboard-dash_cardsCursos-cursos-arriba">
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-barra">
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-izq">
                                <CircularProgress variant="determinate" value={80} />
                            </section>
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-der">
                                <div>
                                    Curso...
                                </div>
                                <div>
                                    80%
                                </div>
                            </section>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-icon">
                            <i class="fas fa-lungs"></i>
                        </div>
                    </section>
                    <section className="dashboard-dash_cardsCursos-cursos-abajo">
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            12
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backUno">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            2
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backDos">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            3
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backTres">
                                <i class="fas fa-gem"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            15
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backCuatro">
                                <i class="fas fa-fire"></i>
                            </div>
                        </div>
                    </section>
                </div>
                <div className="dashboard-dash_cardsCursos-cursos">
                    <section className="dashboard-dash_cardsCursos-cursos-arriba">
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-barra">
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-izq">
                                <CircularProgress variant="determinate" value={80} />
                            </section>
                            <section className="dashboard-dash_cardsCursos-cursos-arriba-barra-der">
                                <div>
                                    Curso...
                                </div>
                                <div>
                                    80%
                                </div>
                            </section>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-arriba-icon">
                            <i class="fas fa-heart"></i>
                        </div>
                    </section>
                    <section className="dashboard-dash_cardsCursos-cursos-abajo">
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            12
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backUno">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            2
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backDos">
                                <i class="fas fa-medal"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            3
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backTres">
                                <i class="fas fa-gem"></i>
                            </div>
                        </div>
                        <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias">
                            15
                            <div className="dashboard-dash_cardsCursos-cursos-abajo-insignias-backCuatro">
                                <i class="fas fa-fire"></i>
                            </div>
                        </div>
                    </section>
                </div>

            </div>
        </div>
    );
}