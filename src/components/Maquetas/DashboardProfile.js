import React from 'react'
import { MainDashboard } from './MainDashboard';
import { NavbarDashboard } from './NavbarDashboard';
import { CardsCarousel } from "./CardsCarousel";
import { CalendarTasks } from './CalendarTasks';
const DashboardProfile = () => {

    return (
      <div className="App dashboard">
          <div className="dashboard-dashboard_container">
              <NavbarDashboard/>
              <MainDashboard/>
              
              <CardsCarousel/>
              <CalendarTasks/>
          </div>
      </div>
    ); 
  }
  
export default DashboardProfile 


