

export function MainDashboard(){
    return (
        <div className="dashboard main">
            <h4 className="main-dash_title">Perfil del estudiante</h4>
            <div className="main-dash_opciones">
                <section clasNames="main-dash_opciones-opciones_uno">
                    <i class="fas fa-search"></i>
                    <input className="main-dash_opciones-opciones_uno-input" placeholder="Buscar"/>
                </section>
                <section className="main-dash_opciones-opciones_dos">
                    <div className="main-dash_opciones-opciones_dos-backIcon">
                        <i class="fas fa-filter"></i>
                    </div>
                    <div className="main-dash_opciones-opciones_dos-select"> 
                        <select className="main-dash_opciones-opciones_dos-select-seleccion">
                            <option value="volvo">Srt By: High Score</option>
                            <option value="saab">Saab</option>
                            <option value="mercedes">Mercedes</option>
                            <option value="audi">Audi</option>
                        </select>
                    </div>
                    <div className="main-dash_opciones-opciones_dos-boton_s">
                        <button className="main-dash_opciones-opciones_dos-select-boton_s-button">
                            <i class="fas fa-plus"></i>
                            Invite Students
                        </button>
                    </div>
                </section>
            </div>
            <div>
            </div>
        </div>
    );
}