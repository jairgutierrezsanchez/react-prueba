import { Routes, Route } from 'react-router-dom';

import Navbar from './components/Navbar'


import CardsCharacters from './components/CardsCharacters'
import ListCharacters from './components/ListCharacters'
import Home from './components/Home'
import Profile from './components/Maquetas/Profile'
import DashboardProfile from './components/Maquetas/DashboardProfile'


function App() {
  return (
    <div className="flex-row">
      <Routes>
      <Route path="/" element={<Navbar />}>
        <Route path="cardCharacters" element={<CardsCharacters />} />
        <Route path="listCharacters" element={<ListCharacters />} />
        <Route path="*" element={<Home />} />
      </Route>
        <Route path="profile" element={<Profile />} />
        <Route path="dashboardProfile" element={<DashboardProfile />} />
    </Routes>
    </div>
  );
}

export default App;
